package gymPackage;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

public class Reasoner
{
    private int keyvalue;
    private State state
    , firstState
    , addressState
    , openingState
    , listMemState
    , listTrainerState
    , registerState
    , isMemberExistState
    , endState;
    private Gym gym;
    private JTextPane txtpnConversation, txtpnInfo;
    private JPanel registerPanel, webBrowserPanel;
    private JWebBrowser webBrowser;
    private JScrollPane scrBackground;
    private String question;
    private Member member;

    //********************************************************************************
    //
    // The main Class Object holding the Domain knowledge
    //
    //********************************************************************************
    
    public Reasoner()
    {
//    	gym = new Gym();
//    	
//    	Member member1 = new Member(111, "Ann", "Williams", 35, "Gold",
//                customDate("05-01-2014"), "member1.jpg");
//        gym.addMember(member1);
//        // 2
//        Member member2 = new Member(112, "Patricia", "White", 30, "Silver",
//                customDate("19-03-2015"), "member2.jpg");
//        gym.addMember(member2);
//
//        // 3
//        Member member3 = new Member(113, "Michael", "Anderson", 33, "Bronze",
//                customDate("01-09-2014"), "member3.jpg");
//        gym.addMember(member3);
//        
//        
//        // 1
//        PersonalTrainer trainer1 = new PersonalTrainer("t01", "Sharon", "Smith",
//                "Aerobic", "I can help you maximise your time and achieve your goals by doing effective gym work or by incorporating aerobic fitness and techniques."
//                , "trainer1.jpg");
//        gym.addTrainer(trainer1);
//
//        // 2
//        PersonalTrainer trainer2 = new PersonalTrainer("t02", "Janet", "Carter", "Swimming",
//                "As a qualified personal trainer and swimming coach I can offer variety. With my experience I know everyones needs are different."
//                , "trainer2.jpg");
//        gym.addTrainer(trainer2);
//
//        // 3
//        PersonalTrainer trainer3 = new PersonalTrainer("t03", "Eric", "Watson", "Body Building",
//                "I appreciate being 'fit' means different things to different people, and with this in mind I make my sessions fun and varied."
//                , "trainer3.jpg");
//        gym.addTrainer(trainer3);
//    	
//    	
//    	TrainingPlan  plan = new TrainingPlan ("p1", "Yoga class", "09-01-2014", member1, trainer2);
//    	gym.addPlans(plan);
//    	plan = new TrainingPlan("p2", "Aerobics class", "15-02-2014", member2, trainer1);
//    	gym.addPlans(plan);
//    	plan = new TrainingPlan("p3", "Swimming class", "25-04-2014", member3, trainer3);
//    	gym.addPlans(plan);
    	
    	
    	
    	// LOAD GYM KNOWLEDGE AND ITS DATA FROM XML
        try {
            openGymAsXML("gym.xml");
            //saveGymToXML("other.xml");
        } catch (IOException e) {
            e.printStackTrace();
        } 
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
	
	//********************************************************************************
	//
	// Create states to respond user request. For example if a user input "address"
	// go to state 2 or if a user input "show address" go to state 3
	//
	//********************************************************************************

        //////////////////////// INITIATE STATES ////////////////////////
        firstState = new State();
        firstState.addKeyword(0, "no");
        firstState.addKeyword(0, "quit");
        
        // ADDRESS
        firstState.addKeyword(2, "address");
        firstState.addKeyword(2, "map");
        firstState.addKeyword(3, "gym map");
        firstState.addKeyword(3, "where is the gym");
        firstState.addKeyword(3, "where is gym");
        firstState.addKeyword(3, "where is power gym");
        firstState.addKeyword(3, "where is the power gym");
        firstState.addKeyword(3, "gym address");
        firstState.addKeyword(3, "address of the gym");
        firstState.addKeyword(3, "your address");
        firstState.addKeyword(3, "show address");
        addressState = new State(); // would you like to know gym address
        addressState.addKeyword(0, "quit");
        addressState.addKeyword(1, "no");
        addressState.addKeyword(3, "yes");

        // OTHERS
        firstState.addKeyword(4, "yes");
        firstState.addKeyword(41, "hi");
        firstState.addKeyword(41, "hello");
        firstState.addKeyword(42, "how are you");
        firstState.addKeyword(43, "show welcome");
        firstState.addKeyword(43, "welcome page");
        firstState.addKeyword(44, "enable navigation"); // ENABLE NAVIGATION BAR
        firstState.addKeyword(45, "disable navigation"); // DISABLE NAVIGATION BAR
        firstState.addKeyword(46, "clear");

        // OPENING TIMES
        firstState.addKeyword(5, "opening");
        firstState.addKeyword(5, "times");
        firstState.addKeyword(5, "open");
        firstState.addKeyword(6, "opening times");
        openingState = new State(); // would you like to know opening times
        openingState.addKeyword(0, "quit");
        openingState.addKeyword(1, "no");
        openingState.addKeyword(6, "yes");

        // LIST ALL MEMBERS
        firstState.addKeyword(9, "members");
        firstState.addKeyword(9, "list");
        firstState.addKeyword(9, "display");
        firstState.addKeyword(10, "list members");
        firstState.addKeyword(10, "list all members");
        firstState.addKeyword(10, "show all members");
        firstState.addKeyword(10, "display all members");
        listMemState = new State(); // would you like to list all members
        listMemState.addKeyword(0, "quit");
        listMemState.addKeyword(1, "no");
        listMemState.addKeyword(10, "yes");

        // LIST ALL PERSONAL TRAINERS
        firstState.addKeyword(11, "trainers");
        firstState.addKeyword(11, "personal trainers");
        firstState.addKeyword(11, "personal");
        firstState.addKeyword(12, "list trainers");
        firstState.addKeyword(12, "list personal trainers");
        firstState.addKeyword(12, "list personaltrainers");
        firstState.addKeyword(12, "list all trainers");
        firstState.addKeyword(12, "list all personal trainers");
        firstState.addKeyword(12, "list all personaltrainers");
        firstState.addKeyword(12, "show trainers");
        firstState.addKeyword(12, "show personal trainers");
        firstState.addKeyword(12, "show personaltrainers");
        firstState.addKeyword(12, "show all trainers");
        firstState.addKeyword(12, "show all personal trainers");
        firstState.addKeyword(12, "show all personaltrainers");
        firstState.addKeyword(12, "display trainers");
        firstState.addKeyword(12, "display personal trainers");
        firstState.addKeyword(12, "display personaltrainers");
        firstState.addKeyword(12, "display all personal trainers");
        firstState.addKeyword(12, "display all personaltrainers");
        listTrainerState = new State(); // would you like to list all trainers
        listTrainerState.addKeyword(0, "quit");
        listTrainerState.addKeyword(1, "no");
        listTrainerState.addKeyword(12, "yes");

        // REGISTER MEMBER
        firstState.addKeyword(13, "register");
        firstState.addKeyword(14, "register member");
        firstState.addKeyword(14, "add member");
        firstState.addKeyword(14, "register a member");
        firstState.addKeyword(14, "add a member");
        firstState.addKeyword(14, "register as a member");
        firstState.addKeyword(14, "want to register");
        registerState = new State(); // would you like to register as new member
        registerState.addKeyword(0, "quit");
        registerState.addKeyword(1, "no");
        registerState.addKeyword(14, "yes");
        
        // DO YOU HAVE MEMBER?
        firstState.addKeyword(15, "do you have member called");
        firstState.addKeyword(15, "do you have member");
        firstState.addKeyword(16, "what is her age?");
        firstState.addKeyword(16, "what is her age");
        isMemberExistState = new State(); // what is her age?
        isMemberExistState.addKeyword(1, "what is her age");
        isMemberExistState.addKeyword(14, "yes");
        
        // WOULD YOU LIKE TO QUIT?
        endState = new State();
        endState.addKeyword(-100, "yes");
        endState.addKeyword(1, "no");
        endState.addKeyword(-100, "quit");
        //////////////////////// END INITIATE STATES ////////////////////////
        
        // SET STARTING STATE
        state = firstState;
        
        txtpnConversation = Gui.txtpnConversation;
        txtpnInfo = Gui.txtpnInfo;
        registerPanel = Gui.registerPanel;
        webBrowserPanel = Gui.webBrowserPanel;
        webBrowser = Gui.webBrowser;
        scrBackground = Gui.scrBackground;
    }

    //********************************************************************************
    //
    // The main brain of the Reasoner class
    // It take user input and executes desired commands
    //
    //********************************************************************************

    public void reason(String aQuestion)
    {
        question = aQuestion;
        append(txtpnConversation,
                "<p style='color:white ;font-size:13px;'><b>You: </b>"
                        + aQuestion + "</p>");
        keyvalue = state.search(aQuestion);
        txtpnInfo.setText("");
        scrBackground.setViewportView(txtpnInfo);

        switch (keyvalue) {
        case 1:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Ok then what else can I do for you?</p>");
            webBrowser
                    .navigate("/home/dava/workspace/eclipse/gymapplication/gymhtml/welcome.html");
            scrBackground.setViewportView(webBrowserPanel);
            state = firstState;
            break;

        // ADDRESS
        case 2:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Would you like to know the gym address?</p>");
            state = addressState;
            break;
        case 3:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Our gym address is "
                            + gym.getAddress() + "</p>");
            webBrowser
                    .navigate("/home/dava/workspace/eclipse/gymapplication/gymhtml/address.html");
            scrBackground.setViewportView(webBrowserPanel);
            state = firstState;
            break;

        // OTHER
        case 4:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Ok what can I do for you?</p>");
            break;
        case 41:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Hello there!!! How can I help you today?</p>");
            break;
        case 42:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>I am great!!! How can I help you today?</p>");
            break;
        case 43:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Hi welcome to Power Gym. How can I help?</p>");
            webBrowser
                    .navigate("/home/dava/workspace/eclipse/gymapplication/gymhtml/welcome.html");
            scrBackground.setViewportView(webBrowserPanel);
            state = firstState;
            break;
        case 44:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Navigation bar is enabled</p>");
            webBrowser.setBarsVisible(true);
            scrBackground.setViewportView(webBrowserPanel);
            break;
        case 45:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Navigation bar is disabled</p>");
            webBrowser.setBarsVisible(false);
            scrBackground.setViewportView(webBrowserPanel);
            break;
        case 46:
            txtpnConversation.setText("<p style='color:white ;font-size:13px;'><b>You: </b>clear</p>");
            break;

        // OPENING TIMES
        case 5:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Would you like to know the opening times?</p>");
            state = openingState;
            break;
        case 6:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Diplaying opening times</p>");
            String openTimes = gym.getOpeningTimes();
            String[] days = openTimes.split(",");
            webBrowser
                    .navigate("http://localhost/workspace/gymhtml/opening.php?"
                            + "&mon=" + days[0] + "&tue=" + days[1] + "&wed="
                            + days[2] + "&thu=" + days[3] + "&fri=" + days[4]
                            + "&sat=" + days[5] + "&sun=" + days[6]);
            scrBackground.setViewportView(webBrowserPanel);
            state = firstState;
            break;

        // LIST MEMBERS
        case 9:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Would you like to list all members?</p>");
            state = listMemState;
            break;
        case 10:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Listing members</p>");
            append(txtpnInfo, ""
                            + "<style type='text/css' media='screen'> body { margin: 0; text-decoration: none; }"
                            + "body table { margin-left: 1px; font-family: verdana; font-size: 10px; color: #FFFF99;} .names { width: 200px;} table td { border: solid 1px black;} </style>"
                            + "<table border='1' cellspacing='10'>");
            Member mem;
            for (int i = 0; i < gym.getSizeMembers(); i++) {
                mem = gym.getMember(i);
                append(txtpnInfo,
                        "<tr> "
                                + " <td><img src='http://localhost/workspace/gymhtml/otherimg/"
                                + mem.getImagePath()
                                + "'alt='' /></td> <td class='names'>"
                                + " Id: " + mem.getId() + " <br/> Name: "
                                + mem.getFullName() + " <br/> Age: "
                                + mem.getAge() + " <br/> Membership type: "
                                + mem.getMemType()
                                + " <br/> Membership expiry date: "
                                + mem.getExpiryDateToString() + "</td></tr>");
            }
            append(txtpnInfo, "</table></body>");
            state = firstState;
            break;

        // LIST PERSONALTRAINERS
        case 11:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Would you like to list all personal trainers?</p>");
            state = listTrainerState;
            break;
        case 12:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Listing personal trainers</p>");
            append(txtpnInfo,
                    ""
                            + "<style type='text/css' media='screen'> body { margin: 0; text-decoration: none; }"
                            + "body table { margin-left: 1px; font-family: verdana; font-size: 10px; color: #99FFCC;} .names { width: 200px;} table td { border: solid 1px black;} </style>"
                            + "<table border='1' cellspacing='10'>");
            for (int i = 0; i < gym.getSizeTrainers(); i++) {
                PersonalTrainer trainer = gym.getTrainer(i);
                append(txtpnInfo,
                        "<tr> <td><img src='http://localhost/workspace/gymhtml/otherimg/"
                                + trainer.getImage()
                                + "'alt='' /></td> <td class='names'>Name: "
                                + trainer.getFullName() + " <br/> Speciality: "
                                + trainer.getSpeciality() + " <br/> Profile: "
                                + "<span style='color:white'>" + trainer.getProfile() + "</span>" + "</td>     </tr>");
            }
            append(txtpnInfo, "</table></body>");
            state = firstState;
            break;

            // REGISTER MEMBER
        case 13:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Would you like to register as a new member?</p>");
            state = registerState;
            break;
        case 14:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Registering a member</p>");
            scrBackground.setViewportView(registerPanel);
            state = firstState;
            break;
            
            // DO YOU HAVE MEMBER
        case 15:
            String subName = question.substring(firstState.getMatchedLength(), question.length());
            subName = subName.replaceAll("\\s","");
            Gym foundMembers = gym.findMembersByName(subName);
            if (foundMembers.getSizeMembers() == 0) {
                append(txtpnConversation,
                        "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b> No such member found...</p>");
            } else {
                append(txtpnInfo, ""
                        + "<style type='text/css' media='screen'> body { margin: 0; text-decoration: none; }"
                        + "body table { margin-left: 1px; font-family: verdana; font-size: 10px; color: #FFFF99;} .names { width: 200px;} table td { border: solid 1px black;} </style>"
                        + "<table border='1' cellspacing='10'>");
                Member tmpMember;
                member = new Member();
                for (int i = 0; i < foundMembers.getSizeMembers(); i++) {
                    tmpMember = foundMembers.getMember(i);
                    
                    append(txtpnInfo,
                            "<tr> "
                                    + " <td><img src='http://localhost/workspace/gymhtml/otherimg/"
                                    + tmpMember.getImagePath()
                                    + "'alt='' /></td> <td class='names'>"
                                    + " Id: " + tmpMember.getId() + " <br/> Name: "
                                    + tmpMember.getFullName() + " <br/> Age: "
                                    + tmpMember.getAge() + " <br/> Membership type: "
                                    + tmpMember.getMemType()
                                    + " <br/> Membership expiry date: "
                                    + tmpMember.getExpiryDateToString() + "</td></tr>");
                }
                member.setAge(foundMembers.getMember(0).getAge());
                append(txtpnInfo, "</table></body>");
                append(txtpnConversation,
                        "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Yes I have found " + foundMembers.getSizeMembers() + " member</p>");
            }
            state = firstState;
            break;
            
        case 16:
        	if(member == null) {
        		append(txtpnConversation,
                        "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Sorry I don't understand, who are you reffering to?</p>");
        	} else {
        		append(txtpnConversation,
        				"<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Her age is: " + member.getAge() + "</p>");
        	}
            
            state = firstState;
            break;
            
        // ERRORS AND QUIT    
        case -1:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Sorry I cannot understand your question...</p>");
            break;
        case 0:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>Are you sure? Would you like to quit the program?</p>");
            state = endState;
            break;
        case -100:
            System.exit(0);
            break;
        default:
            append(txtpnConversation,
                    "<p style='color:00FF33;font-size:13px;'><b>Gymbot: </b>ERROR: Unknown state plz contact admin...</p>");
            break;
        }

    }
    public Gym getGym() {
        return gym;
    }
    public void setGym(Gym gym) {
        this.gym = gym;
    }

    //********************************************************************************
    //
    // These two functions are used for loading and writing xml data.
    //
    //********************************************************************************
    
    public void saveGymToXML(String aFileName) throws IOException {
        XStream xStream = new XStream(new StaxDriver());
        FileOutputStream outFile = new FileOutputStream(aFileName);
        xStream.toXML(gym, outFile);
        outFile.close();
        System.out.println("Saving to XML successful!");
    }

    public void openGymAsXML(String aFileName) throws IOException,
            ClassNotFoundException {
        XStream xStream = new XStream(new StaxDriver());
        FileInputStream inFile = new FileInputStream(aFileName);
        gym = (Gym)xStream.fromXML(inFile);
        inFile.close();
        System.out.println("Opening from XML successful!");
    }

    //********************************************************************************
    //
    // This method add string(HTML) to given JTextPane
    //
    //********************************************************************************

    public void append(JTextPane aTxtPane, String aHTML)
    {
        HTMLEditorKit kit = (HTMLEditorKit) aTxtPane.getEditorKit();
        HTMLDocument doc = (HTMLDocument) aTxtPane.getDocument();
        try {
            kit.insertHTML(doc, doc.getLength(), aHTML, 0, 0, null);
            txtpnConversation.setBackground(new Color(0, 0, 0, 100));
        } catch (BadLocationException ex) {
            ex.printStackTrace();
        } catch (IOException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public Date customDate(String dateString)
    {
        Date date = new Date();
        try {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
        } catch (ParseException pe) {
            System.out.println("ERROR: could not parse date in string \""
                    + dateString + "\"");
        }
        return date;
    }
}
