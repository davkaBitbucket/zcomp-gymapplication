package gymPackage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Member
{
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private String memType;
    private Date memExpiryDate;
    private String imagePath;

    public Member()
    {

    }

    public Member(int anId, String aFirstName, String aLastName,
            Integer anAge, String aMemType, Date aMemExpirydate, String anImagePath)
    {
        id = anId;
        firstName = aFirstName;
        lastName = aLastName;
        age = anAge;
        memType = aMemType;
        memExpiryDate = aMemExpirydate;
        imagePath = anImagePath;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getMemType()
    {
        return memType;
    }

    public void setMemType(String memType)
    {
        this.memType = memType;
    }

    public Date getMemExpiryDate()
    {
        return memExpiryDate;
    }
    
    public String getExpiryDateToString () {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(memExpiryDate);
        return formattedDate;
    }

    public void setMemExpiryDate(Date memExpiryDate)
    {
        this.memExpiryDate = memExpiryDate;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public void setImagePath(String image)
    {
        this.imagePath = image;
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }
}
