<!DOCTYPE html>
<head>
  <meta charset="utf-8">

  <title>Opening Times</title>

  <!-- IMPORT CSS -->
  <link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
  <link href="css/demo.css" rel="stylesheet" type="text/css">

  <!-- IMPORT JAVASCRIPT -->
  <script type="text/javascript" src="javascript/jquery.min.js"></script>
  <script type="text/javascript" src="javascript/jquery.novacancy.js"></script>
  <script type="text/javascript">
   $(document).ready(function() {
     $('.blinkOpening').novacancy({
       'reblinkProbability': 0.1,
       'blinkMin': 0.2,
       'blinkMax': 0.6,
       'loopMin': 8,
       'loopMax': 10,
       'color': 'RED',
       'blink': true
     });
     $('.blinkTimes').novacancy({
       'color': 'WHITE'
     });
   });
  </script>
</head>

<body>

  <div id="openBg">

  <div class="board">
    <p class="blinkOpening">Opening</p>
    <p class="blinkTimes">Times</p>
  </div>
  
  </div>

     <div id="container">
      
      <?php
      $mon = "";
      $tue = "";
      $wed = "";
      $thu = "";
      $fri = "";
      $sat = "";
      $sun = "";

      if (isset($_REQUEST["mon"])) {
	$mon = $_REQUEST["mon"];
	$tue = $_REQUEST["tue"];
	$wed = $_REQUEST["wed"];
	$thu = $_REQUEST["thu"];
	$fri = $_REQUEST["fri"];      
	$sat = $_REQUEST["sat"];      
	$sun = $_REQUEST["sun"];      
      }
      
      ?>

      <table border="1">
	<tr>
	  <td>Monday</td>
	  <td class="times"><?php echo "$mon"?></td>
	</tr>
	<tr>
	  <td>Tuesday</td>
	  <td class="times"><?php echo "$tue"?></td>
	</tr>
	<tr>
	  <td>Wednesday</td>
	  <td class="times"><?php echo "$wed"?></td>
	</tr>
	<tr>
	  <td>Thursday</td>
	  <td class="times"><?php echo "$thu"?></td>
	</tr>
	<tr>
	  <td>Friday</td>
	  <td class="times"><?php echo "$fri"?></td>
	</tr>
	<tr>
	  <td>Saturday</td>
	  <td class="times"><?php echo "$sat"?></td>
	</tr>
	<tr>
	  <td>Sunday</td>
	  <td class="times"><?php echo "$sun"?></td>
	</tr>
      </table>
      
    </div> <!-- end of container -->
  
</body>
</html>
