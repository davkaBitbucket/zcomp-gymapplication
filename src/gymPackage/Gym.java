package gymPackage;

import java.util.ArrayList;
import java.util.List;

//********************************************************************************
//
// Gym class acts as a container class gym domain. 
//
//********************************************************************************

public class Gym 
{
    private String name;
    private String address;
    private String tel;
    private String openingTimes;
    private String gymInfo;
    private List<Member> members;
    private List<PersonalTrainer> trainers;
    private List<TrainingPlan> plans;
    private List<Equipment> equipments;
    
    public Gym() 
    {
        members = new ArrayList<Member>();
        trainers = new ArrayList<PersonalTrainer>();
        plans = new ArrayList<TrainingPlan>();
        equipments = new ArrayList<Equipment>();
    }
    
    public Gym(String aName, String anAddress, String aTel, String anOpeningTimes, String aGymInfo) 
    {
        name = aName;
        address = anAddress;
        tel = aTel;
        openingTimes = anOpeningTimes;
        gymInfo = aGymInfo;
        members = new ArrayList<Member>();
        trainers = new ArrayList<PersonalTrainer>();
        plans = new ArrayList<TrainingPlan>();
        equipments = new ArrayList<Equipment>();
    }
    
    // MEMBER METHODS
    public void addMember(Member aMember) 
    {
        members.add(aMember);
    }
    
    public Member getMember(int index) {
        return members.get(index);
    }
    
    public int getSizeMembers() {
        return members.size();
    }
    
    // FIND MEMBER BY NAME
    public Gym findMembersByName(String aName) {
        Gym tmpGym = new Gym();
        String firstName, lastName;
        for (int i = 0; i < getSizeMembers(); i++) {
            firstName = getMember(i).getFirstName();
            lastName = getMember(i).getLastName();
            if ((firstName + lastName).equalsIgnoreCase(aName)) {
                tmpGym.addMember(getMember(i));
                return tmpGym;
            }
            if (firstName.equalsIgnoreCase(aName)) {
                tmpGym.addMember(getMember(i));
            }
            if (lastName.equalsIgnoreCase(aName)) {
                tmpGym.addMember(getMember(i));
            }
        }
        return tmpGym;
    }
    
    // TRAINER METHODS
    public void addTrainer(PersonalTrainer aTrainer) 
    {
        trainers.add(aTrainer);
    }
    
    public PersonalTrainer getTrainer(int index) {
        return trainers.get(index);
    }
    
    public int getSizeTrainers() {
        return trainers.size();
    }
    
    // TRAINING PLAN
    public void addPlans(TrainingPlan aPlan) {
        plans.add(aPlan);
    }
    
    public void addEquipment(Equipment anEquipment) {
        equipments.add(anEquipment);
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getOpeningTimes() {
        return openingTimes;
    }
    public void setOpeningTimes(String openingTimes) {
        this.openingTimes = openingTimes;
    }
    public String getGymInfo() {
        return gymInfo;
    }
    public void setGymInfo(String gymInfo) {
        this.gymInfo = gymInfo;
    }
    
}
