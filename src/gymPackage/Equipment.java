package gymPackage;

public class Equipment 
{
    private String name;
    private String location;
    private boolean isAvailable;
    
    public Equipment() {
        
    }
    
    public Equipment(String aName, String aLocation, boolean avail) {
    	name = aName;
    	location = aLocation;
    	isAvailable = avail;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public boolean isAvailable() {
        return isAvailable;
    }
    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }
}
