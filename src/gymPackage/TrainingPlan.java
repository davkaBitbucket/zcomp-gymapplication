
package gymPackage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrainingPlan 
{
    private String id;
    private String name;
    private Date date;
    private Member member;
    private PersonalTrainer trainer;
    
    public TrainingPlan() {
        
    }
    
    public TrainingPlan(String anId, String aName, String txtDate
    		, Member aMember, PersonalTrainer aTrainer) {
    	id = anId;
    	name = aName;
    	date = customDate(txtDate);
    	member = aMember;
    	trainer = aTrainer;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    
	public Date customDate(String dateString) {
    	Date date = new Date();
    	try {
    		date = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
    	} catch (ParseException pe) {
    		System.out.println("ERROR: could not parse date in string \""
    				+ dateString + "\"");
    	}
    	return date;
    }
}
