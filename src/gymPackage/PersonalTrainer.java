
package gymPackage;

public class PersonalTrainer 
{
    private String id;
    private String firstName;
    private String lastName;
    private String speciality;
    private String profile;
    private String image;
    
    public PersonalTrainer() {
        
    }
    
    public PersonalTrainer(String anId, String aFirstName, 
            String aLastName, String aSpeciality, String aProfile, String anImage) {
        id = anId;
        firstName = aFirstName;
        lastName = aLastName;
        speciality = aSpeciality;
        profile = aProfile;
        image = anImage;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }
    
    public String getSpeciality() {
        return speciality;
    }
    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }
    public String getProfile() {
        return profile;
    }
    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }
}
